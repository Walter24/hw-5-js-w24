let hero = {
    name: "Рокі",
    "last name": "Бальбоа",
    age: 33,
    "body features": {
        height: 178,
        "body type": "качок",
        "body colors": {
            "eyes color": "сапфірові",
            "hair color": "вугільно чорні",
            "skin color": "загорілий"
        }
    },
    hobby: "виживати",
    "family status": "ніколи",
    credo: "недочекаєтесь",
    friends: null     // тут потрібно врахувати null бо в JS признана помилка: typeof null = "object"
};


function clonedObj(obj) {
    let result = {};    // створили пустий об'єкт (клон)
    for(let key in obj){
        if (typeof obj[key] == "object" && obj[key] !== null) { // спрацює коли свойство є об'єкт (має рівень вкладеності)
            result[key] = clonedObj(obj[key]);   // скопіювати усі свойства об'єкта hero в clonedHero
        } else {
        result[key] = obj[key]; // спрацює коли свойство не має рівня вкладеності: скопіювати усі свойства об'єкта в клонований об'єкт
        }
   
    }
    return result;
}

let clonedHero = clonedObj(hero);   

// для перевірки чи склонований об'єкт є вже незалежний окремий об'єкт:
clonedHero.name = "Фродо";
clonedHero["last name"] = "Беггінс";
clonedHero.age = 123;
clonedHero.hobby = "золоті каблучки";
clonedHero["body features"].height = 105;
clonedHero["body features"]["body type"] = "хобіт";
clonedHero["body features"]["body colors"]["eyes color"] = "голубі";
clonedHero["body features"]["body colors"]["hair color"] = "чорні";
clonedHero["body features"]["body colors"]["skin color"] = "бліда-бліда";
clonedHero["family status"] = null;
clonedHero.credo = "кільце по-любому моє";
clonedHero.friends = ["Сем", "Перегрин", "Мері", "Араґорн", "Ґендельф", "Леґолас", "Гімлі", "Боромир", "Галадріель"];

console.log("hero - ", hero);

console.log("clonedHero - ", clonedHero);